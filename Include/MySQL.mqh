//+------------------------------------------------------------------+
//|                                                        MySQL.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, Solomatov Sergey"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>
//+----------------------------------------------------------------------------+
//|                                                             mql4-mysql.mqh |
//+----------------------------------------------------------------------------+
//|                                                      Built by Sergey Lukin |
//|                                                    contact@sergeylukin.com |
//|                                                                            |
//| This libarry is highly based on following:                                 |
//|                                                                            |
//| - MySQL wrapper by "russel": http://codebase.mql4.com/5040                 |
//| - MySQL wrapper modification by "vedroid": http://codebase.mql4.com/8122   |
//| - EAX Mysql: http://www.mql5.com/en/code/855                               |
//| - This thread: http://forum.mql4.com/60708 (Cheers to user "gchrmt4" for   |
//|   expanded explanations on how to deal with ANSI <-> UNICODE hell in MQL4  |
//|                                                                            |
//+----------------------------------------------------------------------------+
#property copyright "Unlicense"
#property link      "http://unlicense.org/"
 
#import "kernel32.dll"
   int lstrlenA(int);
   void RtlMoveMemory(uchar & arr[], int, int);
   int LocalFree(int); // May need to be changed depending on how the DLL allocates memory
#import
 
#import "msvcrt.dll"
  // TODO extend/handle 32/64 bit codewise
  int memcpy(char &Destination[], int Source, int Length);
  int memcpy(char &Destination[], long Source, int Length);
  int memcpy(int &dst,  int src, int cnt);
  int memcpy(long &dst,  long src, int cnt);  
#import
 
#import "libmysql.dll"
int     mysql_init          (int db);
int     mysql_errno         (int db);
int     mysql_error         (int db);
int     mysql_real_connect  (int db, uchar & host[], uchar & user[], uchar & password[], uchar & db[], int port, int socket, int clientflag);
int     mysql_real_query    (int db, uchar & query[], int length);
int     mysql_query         (int db, uchar & query[]);
void    mysql_close         (int db);
int     mysql_store_result  (int db);
int     mysql_use_result    (int db);
int     mysql_insert_id     (int db);
 
int     mysql_fetch_row     (int resultStruct);
int     mysql_fetch_field   (int resultStruct);
int     mysql_fetch_lengths (int resultStruct);
int     mysql_fetch_field_direct(int resultStruct, uint i);
int     mysql_num_fields    (int resultStruct);
int     mysql_num_rows      (int resultStruct);
void    mysql_free_result   (int resultStruct);
#import


class MySQLField : public CObject
{
   public:
      string key;
      string value;
      int modified;
      MySQLField() { modified=0; };
};

class MySQLRow : public CObject
{
   public:
      CArrayObj* fields;
      int changed;
      int updated;
      MySQLRow()    { fields = new CArrayObj(); changed =0; updated = 0; };
      ~MySQLRow()   { delete(fields); };
};

class MySQL
{
   public:
      MySQL();
      MySQL(int );
     ~MySQL();
   private:
      int db;
      string Host;
      string User;
      string Password;
      string Database;
      string Tablename; 
      int socket;
      int client_flag;           
      int results;
      int errorno;
      CArrayObj* m_rows;
   private:
      void clear();
      bool Init();
      void Deinit();
      int GetLastError();
      string InsertQuery(string TABLE, string FIELDS, string VALUES);
      string UpdateQuery(string TABLE, string SET, string WHERE = NULL, string ORDER_BY = NULL, string LIMIT = NULL);
      string SelectQuery(string SELECT, string FROM, string WHERE = NULL, string GROUP_BY = NULL, string HAVING = NULL, string ORDER_BY = NULL, string LIMIT = NULL);
      string Escape(string value);
      void   error();
      void   error(string sqlin);
   public:
      bool     Connect(string host, string user, string password, string dbName, uint port = 3306);
      bool     Connect();
      /*bool Select();
      bool Insert();
      bool Delete();
      bool Update();*/
      bool     NoError();
      bool     Query(string query);
      string   get(string key, int index);        
      void     AddNew(string table);
      void     select(string tablename);
      void     set(string, string);
      string   get_primary_key(string strTable);
      int      write();
      int      read_rows(string SQL);
      int      ReadRows();
};

MySQL::MySQL()
{
   if(MQLInfoInteger(MQL_DLLS_ALLOWED)==0)
   {
      Alert("DLL calling not allowed. Allow and try again!");
   }
   
   if (!Init())
   {
      Alert("MySQL is not initialized!");
   }
}

MySQL::MySQL(int dbMySQL)
{
   this.db = dbMySQL;
   Init();
}

MySQL::~MySQL(void)
{
   Deinit();
}

void MySQL::Deinit()
{
   
   clear();
   delete m_rows;
   m_rows = NULL;
   
   if (db != 0)
   {
      mysql_close(db);
      db = 0;
   }
}

bool MySQL::Init()
{
   //Deinit();
   
   if (db == 0)
   {
      db = mysql_init(db);
   }
    
   if ( db == 0 )
   {
      Print("init_MySQL: mysql_init failed. There was insufficient memory to allocate a new object");
      return (false);
   }
   // ��������� ���������� �� �������
   client_flag = 0;
   socket = NULL;
   m_rows = new CArrayObj();
   
   return (true);
}

bool MySQL::Connect(void)
{
   Init();
   return (this.Connect(this.Host, this.User, this.Password, this.Database));
}

bool MySQL::Connect(string host, string user, string password, string database, uint port = 3306)
{
   Init();
   // Convert the strings to uchar[] arrays
   this.Host = host;
   this.User = user;
   this.Password = password;
   this.Database = database;
   
   uchar hostChar[];
   StringToCharArray(Host, hostChar);
   uchar userChar[];
   StringToCharArray(User, userChar);
   uchar passwordChar[];
   StringToCharArray(Password, passwordChar);
   uchar databaseChar[];
   StringToCharArray(Database, databaseChar);
    
   int result = mysql_real_connect(db, hostChar, userChar, passwordChar, databaseChar, port, socket, client_flag); 
   
   if ( result != db )
   {
      int errno = mysql_errno(db);
      string error = mql4_mysql_ansi2unicode(mysql_error(db));
      
      Print("Connect: mysql_errno: ", errno,"; mysql_error: ", error);
      return (false);
   }
   else
   {
      int errno = mysql_errno(db);
      string error = mql4_mysql_ansi2unicode(mysql_error(db));
      
      Print("Connect: mysql_errno: ", errno,"; mysql_error: ", error);
   }
   
   return (true);
}

//+----------------------------------------------------------------------------+
//| Check whether there was an error with last query                           |
//|                                                                            |
//| return (true): no error; (false): there was an error;                      |
//+----------------------------------------------------------------------------+
int MySQL::GetLastError()
{
   errorno = mysql_errno(db);
   
   return(errorno);
}
bool MySQL::NoError()
{
   int errno = this.GetLastError();
   string error = mql4_mysql_ansi2unicode(mysql_error(db));
   
   if ( errno > 0 )
   {
      Print("MySQL_NoError: mysql_errno: ", errno,"; mysql_error: ", error);
      return (false);
   }
   
   return (true);
}

bool MySQL::Query(string query)
{
   if (StringLen(query) == 0)
   {
      Print("Query: ������� ��������� ������ ������");
      return (false);
   }
   uchar queryChar[];
   StringToCharArray(query, queryChar);
   
   mysql_query(db, queryChar);
   if (NoError())
   {
      return (true);
   }
   
   return (false);
}

string MySQL::Escape(string strInput)
{
    StringReplace(strInput, "'", "\'");
    return strInput;
}

string MySQL::SelectQuery(string SELECT, string FROM, string WHERE = NULL, string GROUP_BY = NULL, string HAVING = NULL, string ORDER_BY = NULL, string LIMIT = NULL)
{
   string query = StringConcatenate(
      "SELECT ", SELECT, " ",
      "FROM ", FROM, " "
   );
   query = addMysqlParametrs(query, WHERE, GROUP_BY, HAVING, ORDER_BY, LIMIT);
   return (query);
}
string MySQL::InsertQuery(string TABLE, string FIELDS, string VALUES)
{
   string query = StringConcatenate(
      "INSERT INTO `",
      TABLE, "`", 
      " ( ", FIELDS, ")",
      " VALUES (", VALUES, ");");
   return(query);
}
string MySQL::UpdateQuery(string TABLE, string SET, string WHERE = NULL, string ORDER_BY = NULL, string LIMIT = NULL)
{
   string query = StringConcatenate("UPDATE `", TABLE, "` SET ", SET);
   query = addMysqlParametrs(query, WHERE, NULL, NULL, ORDER_BY, LIMIT);
   
   return query;
}

string MySQL::get(string strKey, int iValue)
{
   if (m_rows.Total() >= iValue)
   {
      MySQLRow* pRow = m_rows.At(iValue);
      CArrayObj* pFields = pRow.fields;
      
      for (int i=0; i< pFields.Total(); i++)
      {
         MySQLField *pField = pFields.At(i);
         if (pField.key == strKey)
         {
            return pField.value;
         }        
      }
   }
   
   return "";      
}


void MySQL::clear()
{
   if (m_rows == NULL) return;
   for (int i = 0; i < m_rows.Total(); i++)
   {
      MySQLRow *pRow = m_rows.At(i);
      delete pRow;        
   }
   m_rows.Clear();
}


void MySQL::AddNew(string strTable)
{
   this.clear();
   this.Tablename = strTable;        
}

void MySQL::set(string strKey, string strValue)
{
   MySQLRow* pRow;
   if (m_rows.Total() == 0)
   {
      pRow = new MySQLRow();
      m_rows.Add(pRow);
   }
   
   pRow = m_rows.At(0);
   pRow.changed = 1;
   
   CArrayObj* pFields = pRow.fields;    
   for (int i = 0; i < pFields.Total(); i++)
   {
      MySQLField* pField = pFields.At(i);
      if (pField.key == strKey)
      {
         pField.value = strValue;
         pField.modified = 1;            
         return;
      }        
   }
   
   MySQLField* pField = new MySQLField();
   pField.key = strKey;
   pField.value = strValue;
   pField.modified = 1;
   
   pRow.fields.Add(pField);
   
   return;
}

void MySQL::select(string tablename)
{
   this.Tablename = tablename;
}

int MySQL::ReadRows()
{
   this.clear();
   int num_rows = 0;
   int result = mysql_store_result(db); // ���������� ���� ��������� �������
   if (result > 0)
   {
          num_rows   = mysql_num_rows(result);  // �������� ���������� ����� � �������
      int num_fields = mysql_num_fields(result);// �������� ���������� �������� � �������
      if (num_rows == 0) // ���� ��� � ������� ��� �����, ����� �� ����, ���������� 0 �����
      {
         return (num_rows);
      }
      string field_name[];
      ArrayResize(field_name, num_fields);
      int sizeInt = sizeof(int);
      
      for (int i = 0; i < num_rows; i++)
      {
         int row_ptr = mysql_fetch_row(result);
         int len_ptr = mysql_fetch_lengths(result);
         MySQLRow* pRow = new MySQLRow();
         
         for ( int j = 0; j < num_fields; j++ )
         {
            MySQLField* pField = new  MySQLField();
            if (i == 0)
            {
               // ����������� �������� ������
               int colum_ptr = mysql_fetch_field(result);
               int colum_ptr_name = 0;
               memcpy(colum_ptr_name, colum_ptr, sizeInt);
               field_name[j] = mql4_mysql_ansi2unicode(colum_ptr_name);
               //LocalFree(colum_ptr_name);
            }
            
            // ����������� ���������� ������
            //int sdfsdf_ptr = 0;
            //int lenght = 0;
            //memcpy(sdfsdf_ptr, row_ptr + j * sizeInt, sizeInt);
            //memcpy(lenght, len_ptr + j * sizeInt, sizeInt);
            //string qwer = mql4_mysql_ansi2unicode2(sdfsdf_ptr, lenght);
            
            int field_ptr = 0;
            memcpy(field_ptr, row_ptr + j * sizeInt, sizeInt);
            string field = mql4_mysql_ansi2unicode(field_ptr);
            pField.key = field_name[j];
            pField.value = field;
            pField.modified = 0;
            pRow.fields.Add(pField);
            pRow.updated = 1;
         }
         
         m_rows.Add(pRow);
      }
      mysql_free_result(result);
   }
   
   return (num_rows);
}

int MySQL::read_rows(string SQL)
{
   uchar SQLchar[];
   StringToCharArray(SQL, SQLchar);
   mysql_real_query(db, SQLchar, StringLen(SQL));
   this.errorno = mysql_errno(db);
   if (errorno)
   {
       this.error(SQL);
   }
   
   this.clear();
   
   int result = mysql_store_result(db); // ���������� ���� ��������� �������
   int num_rows = 0;
   if (result > 0)   // ���� ���� ��� ������������
   {
      num_rows       = mysql_num_rows(result);  // �������� ���������� ����� � �������
      int num_fields = mysql_num_fields(result);// �������� ���������� �������� � �������
      if (num_rows == 0) // ���� ��� � ������� ��� �����, ����� �� ����, ���������� 0 �����
      {
          return (num_rows);
      }
      
      string field_name[];
      ArrayResize(field_name, num_fields);
      int sizeInt = sizeof(int);
      
      for (int i = 0; i < num_rows; i++)
      {
         int row_ptr = mysql_fetch_row(result);
         int len_ptr = mysql_fetch_lengths(result);
         MySQLRow* pRow = new MySQLRow();
         
         for ( int j = 0; j < num_fields; j++ )
         {
            MySQLField* pField = new  MySQLField();
            // ����������� �������� ������
            if (i == 0)
            {
               int colum_ptr = mysql_fetch_field(result);
               int colum_ptr_name = 0;
               memcpy(colum_ptr_name, colum_ptr, sizeInt);
               field_name[j] = mql4_mysql_ansi2unicode(colum_ptr_name);
            }
            // ����������� ���������� ������
            //int sdfsdf_ptr = 0;
            //int lenght = 0;
            //memcpy(sdfsdf_ptr, row_ptr + j * sizeInt, sizeInt);
            //memcpy(lenght, len_ptr + j * sizeInt, sizeInt);
            //string qwer = mql4_mysql_ansi2unicode2(sdfsdf_ptr, lenght);
            int field_ptr = 0;
            memcpy(field_ptr, row_ptr + j * sizeInt, sizeInt);
            string field = mql4_mysql_ansi2unicode(field_ptr);
            
            pField.key = field_name[j];
            pField.value = field;
            pField.modified = 0;
            pRow.fields.Add(pField);
            pRow.updated = 1;
         }
         
         m_rows.Add(pRow);
      }
      mysql_free_result(result);
   }
   
   return num_rows;
}


string MySQL::get_primary_key(string strTable)
{
   MySQL *__db_int = new MySQL(this.db);
   string strSQL = "SHOW KEYS FROM `"+strTable+"` WHERE Key_name='PRIMARY'";
   __db_int.read_rows(strSQL);
   string ret = __db_int.get("Column_name",0);
   delete __db_int;
   
   return ret;
}


int MySQL::write()
{
   // Iterate over the current resultset
   for (int i = 0; i < m_rows.Total(); i++)
   {
      MySQLRow* pRow = m_rows.At(i);
      
      // nothing to do, if row not modified        
      if (pRow.changed == 0)
      {
         continue;
      }
      
      CArrayObj* pFields = pRow.fields;
      string SQL;
      uchar SQLchar[];
      int k = 0;
      this.errorno = 0;
      if (pRow.updated == 0)
      {
         string strFields;
         string strValues;
         for (k = 0; k < pFields.Total(); k++)
         {
            MySQLField* pField = pFields.At(k);
            if (pField.modified == 1)
            {
               strFields = strFields + "`" + pField.key   + "`,";
               strValues = strValues + "'" + Escape(pField.value) + "',";
            }
         }
         strFields = StringSubstr(strFields,0,StringLen(strFields)-1);
         strValues = StringSubstr(strValues,0,StringLen(strValues)-1);
         
         //string strSQL;
         SQL = this.InsertQuery(this.Tablename, strFields, strValues);
         
         StringToCharArray(SQL, SQLchar);
         
         mysql_real_query(db, SQLchar, StringLen(SQL));
         this.errorno = mysql_errno(db);
         if (this.errorno != 0)
         {
            this.error(SQL);
         }
      }
      else
      {
         string pk = this.get_primary_key(this.Tablename);
         string strData = "";
         string strWhere = "";
         for (k = 0; k < pFields.Total(); k++)
         {                
            MySQLField* pField = pFields.At(k);
            
            if (pField.key == pk)
            {
               strWhere = "`" + pk + "`='" + Escape(pField.value) + "'";
            }
            
            if (pField.modified == 1)
            {
               strData = strData + "`" + pField.key + "`='" + Escape(pField.value)+"',";
            }                
         }
         strData = StringSubstr(strData,0,StringLen(strData)-1);
         SQL = UpdateQuery(this.Tablename, strData, strWhere);
         
         StringToCharArray(SQL, SQLchar);
         
         mysql_real_query(db, SQLchar, StringLen(SQL));
         this.errorno = mysql_errno(db);
         if (this.errorno != 0)
         {
            this.error(SQL);
         }
      }
   }
   return 0;
}

void MySQL::error()
{
   string s = mql4_mysql_ansi2unicode(mysql_error(db));
   Alert("Error: ", s);
}

void MySQL::error(string sqlin)
{
   string s = mql4_mysql_ansi2unicode(mysql_error(db));
   Alert("Error: ", s);
   Alert("SQL Input: ", sqlin);
}

string addMysqlParametrs(string query, string WHERE = NULL, string GROUP_BY = NULL, string HAVING = NULL, string ORDER_BY = NULL, string LIMIT = NULL)
{
   if (WHERE != NULL)
   {
      query = StringConcatenate(query, " WHERE ", WHERE);
   }
   if (GROUP_BY != NULL)
   {
      query = StringConcatenate(query, " GROUP_BY ", GROUP_BY);
   }
   if (HAVING != NULL)
   {
      query = StringConcatenate(query, " HAVING ", HAVING);
   }
   if (ORDER_BY != NULL)
   {
      query = StringConcatenate(query, " ORDER_BY ", ORDER_BY);
   }
   if (LIMIT != NULL)
   {
      query = StringConcatenate(query, " LIMIT ", LIMIT);
   }
   
   return query;
}


//+----------------------------------------------------------------------------+
//| Lovely function that helps us to get ANSI strings from DLLs to our UNICODE |
//| format                                                                     |
//| http://forum.mql4.com/60708                                                |
//+----------------------------------------------------------------------------+
string mql4_mysql_ansi2unicode(int ptrStringMemory)
{
   if (ptrStringMemory <= 0)  return "";
   
   int szString = lstrlenA(ptrStringMemory);
   if (szString <= 0) return "";
   
   uchar ucValue[];
   ArrayResize(ucValue, szString + 1);
   RtlMoveMemory(ucValue, ptrStringMemory, szString + 1);
   string str = CharArrayToString(ucValue);
   //LocalFree(ptrStringMemory);
   //Print(ptrStringMemory);
   return str;
}
//+----------------------------------------------------------------------------+

string mql4_mysql_ansi2unicode2(int ptrStringMemory, int szString)
{
  uchar ucValue[];
  ArrayResize(ucValue, szString + 1);
  if (szString == 19)
  {
   szString = 0;
  }
  RtlMoveMemory(ucValue, ptrStringMemory, szString + 1);
  string str = CharArrayToString(ucValue);
  LocalFree(ptrStringMemory);
  return str;
}